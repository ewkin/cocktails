import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {Avatar, IconButton, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {logoutRequest} from "../../../../store/actions/usersActions";
import {historyPush} from "../../../../store/actions/historyActions";

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  }
}));


const UserMenu = ({user}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  return (
    <>
      <IconButton
        color={'inherit'}
        onClick={handleClick}>
        <Avatar
          className={classes.avatar}
          alt="User avatar"
          sizes={2}
          src={user.avatar}/>
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem disabled>{user.displayName}</MenuItem>
        {user.role === 'admin' ?
          <MenuItem onClick={() => dispatch(historyPush('/cocktail/review'))}>Review Cocktails</MenuItem> :
          <MenuItem onClick={() => dispatch(historyPush('/cocktail/my'))}> My cocktails</MenuItem>
        }

        <MenuItem onClick={() => dispatch(historyPush('/cocktail/add'))}>Add Cocktail</MenuItem>
        <MenuItem onClick={() => dispatch(logoutRequest())}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;