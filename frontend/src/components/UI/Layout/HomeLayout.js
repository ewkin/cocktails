import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import AppDrawer from "./AppDrawer";


const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(1)
  },
  titleLink: {
    textDecoration: "none"
  }
}))

const HomeLayout = ({children}) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppDrawer/>
      <div className={classes.content}>
        {children}
      </div>
    </div>
  );
};

export default HomeLayout;