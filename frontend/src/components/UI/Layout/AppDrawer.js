import React from 'react';
import {Drawer, makeStyles, MenuItem, MenuList, Toolbar} from "@material-ui/core";
import {Link, useParams} from "react-router-dom";
import {useSelector} from "react-redux";


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  }
}));


const AppDrawer = () => {
  const params = useParams();
  const classes = useStyles();
  const categories = useSelector(state => state.cocktails.categories);


  return (
    <Drawer
      className={classes.drawer}
      classes={{paper: classes.drawerPaper}}
      variant={"permanent"}
      open>
      <Toolbar/>
      <MenuList>
        <MenuItem
          component={Link}
          to={'/'}
          selected={!params.title}
        >All Cocktails
        </MenuItem>
        {categories.map(category => (
          <MenuItem
            component={Link}
            to={`/category/${category.title}`}
            key={category.title}
            selected={category.title === params.title}
          >{category.title}
          </MenuItem>
        ))}
      </MenuList>
    </Drawer>
  );
};

export default AppDrawer;