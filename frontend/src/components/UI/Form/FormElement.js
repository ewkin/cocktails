import React from 'react';
import PropTypes from 'prop-types';
import {Grid, MenuItem, TextField} from "@material-ui/core";

const FormElement = ({
                       select,
                       options,
                       error,
                       ...props
                     }) => {
  let inputChildren = null;


  if (select) {
    inputChildren = options.map(option => (
      <MenuItem key={option.title} value={option.title}>
        {option.title}
      </MenuItem>
    ));
  }

  return (
    <Grid item xs>
      <TextField
        select={select}
        error={Boolean(error)}
        helperText={error}
        {...props}
      >
        {inputChildren}
      </TextField>
    </Grid>
  );
};

FormElement.propTypes = {
  ...TextField.propTypes,
  options: PropTypes.arrayOf(PropTypes.object),
  error: PropTypes.string,
  select: PropTypes.bool

};

export default FormElement;