import React from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import imageNotAvailable from '../../assets/images/notAvailable.png';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Button} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {fetchCocktailApprove, fetchCocktailDelete} from "../../store/actions/cocktailsActions";

const useStyles = makeStyles({
  card: {
    height: '100%',
    borderTop: '1px solid black'
  },
  media: {
    maxHeight: '250px',
    maxWidth: '250px',
    paddingTop: 20,
  }
});

const CocktailView = ({name, recipe, published, admin, ingredients, image, type, id}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  let cardImage = imageNotAvailable;
  if (image) {
    cardImage = image;
  }
  let actions = null;
  if (type !== 'public') {
    if (admin) {
      actions =
        <Grid item xs={12}>
          <Button onClick={() => dispatch(fetchCocktailApprove({id: id}))} variant="contained" color="primary"
                  disabled={published}>Approve</Button>
          <Button onClick={() => dispatch(fetchCocktailDelete(id))} variant="contained"
                  color="secondary">Delete</Button>
        </Grid>
    } else {
      actions = published ? (<p>Post was approved</p>) : (<p>Post Under Moderation</p>)
    }
  }


  return (
    <Grid item container direction="row" className={classes.card}>
      <Grid item xs={4}>
        <img className={classes.media} src={cardImage} alt={name}/>
      </Grid>
      <Grid item xs={8}>
        <h2>{name}</h2>
        <p>Ingredients:</p>
        <ul>
          {ingredients.map(ingredient => (
            <li key={ingredient._id}>{ingredient.title} - {ingredient.amount} ml</li>
          ))}
        </ul>
      </Grid>
      <Grid item xs={12}>
        <p>Recipe:</p>
        {recipe}
      </Grid>
      {actions}
    </Grid>
  );
};

CocktailView.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  recipe: PropTypes.string.isRequired,
  ingredients: PropTypes.array.isRequired,
  image: PropTypes.string,
  admin: PropTypes.bool.isRequired,
};

export default CocktailView;