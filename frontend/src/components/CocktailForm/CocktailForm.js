import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FileInput from "../UI/Form/FileInput";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {Button, IconButton} from "@material-ui/core";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(2),
  },
}));

const CocktailForm = ({onSubmit, categories, error, loading}) => {
  const classes = useStyles();

  const [state, setState] = useState({
    name: '',
    recipe: '',
    ingredients: [{title: '', amount: ''}],
    image: '',
    category: ''
  });

  const submitFormHandler = e => {
    e.preventDefault();
    onSubmit(state);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.files[0];
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const ingredientChangeHandler = (i, name, value) => {
    setState(prevState => ({
      ...prevState, ingredients:
        prevState.ingredients.map((ing, idx) => {
          if (idx === i) {
            return {
              ...prevState.ingredients[i], [name]: value
            }
          }
          return ing
        })
    }))
  };

  const deleteIngredient = (i) => {
    setState(prevState => ({
      ...prevState, ingredients: prevState.ingredients.filter((value, index) => index !== i)
    }))
  };

  const addIngredient = () => {
    setState(prevState => ({
      ...prevState, ingredients: [...prevState.ingredients, {title: '', amount: ''}]
    }))
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }


  return (
    <form onSubmit={submitFormHandler} noValidate>
      <Grid container direction="column" spacing={2}>
        <FormElement
          required
          select
          label="Category"
          name="category"
          value={state.category}
          onChange={inputChangeHandler}
          options={categories}
          error={getFieldError('category')}
        />
        <FormElement
          required
          label="Name"
          name="name"
          value={state.name}
          onChange={inputChangeHandler}
          error={getFieldError('name')}

        />
        <FormElement
          required
          label="Recipe"
          name="recipe"
          value={state.recipe}
          onChange={inputChangeHandler}
          error={getFieldError('recipe')}
        />
        <Grid item xs>
          {state.ingredients.map((ing, i) => (
            <Grid key={i} container spacing={2} justify={"space-between"} alignItems='center'>
              <Grid item xs={8}>
                <FormElement
                  required
                  label="Title"
                  onChange={e => ingredientChangeHandler(i, 'title', e.target.value)}
                  error={getFieldError('ingredients')}
                />
              </Grid>
              <Grid item xs={3}>
                <FormElement
                  key={i}
                  required
                  type="number"
                  label="Amount"
                  onChange={e => ingredientChangeHandler(i, 'amount', e.target.value)}
                  error={getFieldError('ingredients')}
                />
              </Grid>
              <Grid item xs={1}>
                <IconButton disabled={state.ingredients.length === 1} onClick={() => deleteIngredient(i)}
                            color="secondary"><HighlightOffIcon/></IconButton>
              </Grid>
            </Grid>
          ))}
          <Button className={classes.button} onClick={addIngredient} variant="outlined" color="primary">Add
            Ingredient</Button>
        </Grid>
        <Grid item xs>
          <FileInput
            name='image'
            label='Image'
            onChange={fileChangeHandler}
            error={getFieldError('image')}
          />
        </Grid>
        <Grid item xs>
          <ButtonWithProgress type="submit" color="primary" variant="contained" startIcon={<CloudUploadIcon/>}
                              loading={loading} disabled={loading}>
            Post
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </form>
  );
};

export default CocktailForm;