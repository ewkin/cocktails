import React, {useEffect} from 'react';
import HomeLayout from "../../components/UI/Layout/HomeLayout";
import {useParams} from "react-router-dom";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import {fetchCocktailsReviewRequest} from "../../store/actions/cocktailsActions";
import {Helmet} from "react-helmet";
import Grid from "@material-ui/core/Grid";
import {CircularProgress, Typography} from "@material-ui/core";
import CocktailView from "../../components/CocktailView/CocktailView";


const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  }
}));

const Review = () => {
  const params = useParams()
  const classes = useStyles();
  const dispatch = useDispatch();
  const cocktails = useSelector(state => state.cocktails.cocktails);
  const loading = useSelector(state => state.cocktails.cocktailsLoading);
  const user = useSelector(state => state.users.user);
  const categories = useSelector(state => state.cocktails.categories);
  const currentCategory = categories.find(c => c.title === params.title);
  const currentName = currentCategory ? currentCategory.title : 'Review Cocktails';

  useEffect(() => {
    dispatch(fetchCocktailsReviewRequest(params.title));
  }, [dispatch, params.title]);

  return (
    <HomeLayout>
      <Helmet><title>{currentName}</title></Helmet>
      <Grid container direction="column" spacing={2}>
        <Grid item container direction="row" justify="space-between" alignItems="center">
          <Grid item>
            <Typography variant='h4'>{currentName}</Typography>
          </Grid>
        </Grid>
        <Grid item container direction="column" spacing={1}>
          {loading ? (
            <Grid container justify='center' alignContent="center" className={classes.progress}>
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>) : cocktails.map(cocktail => (
            <CocktailView
              key={cocktail._id}
              id={cocktail._id}
              name={cocktail.name}
              recipe={cocktail.recipe}
              published={cocktail.published}
              ingredients={cocktail.ingredients}
              admin={user ? user.role === 'admin' : false}
              image={cocktail.image}
            />
          ))}
        </Grid>
      </Grid>
    </HomeLayout>
  );
};

export default Review;