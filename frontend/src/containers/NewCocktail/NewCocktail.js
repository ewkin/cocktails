import React from 'react';
import Grid from "@material-ui/core/Grid";
import {Typography} from "@material-ui/core";
import CocktailForm from "../../components/CocktailForm/CocktailForm";
import {useDispatch, useSelector} from "react-redux";
import {createCocktailRequest} from "../../store/actions/cocktailsActions";

const NewCocktail = () => {
  const dispatch = useDispatch();
  const error = useSelector(state => state.cocktails.createCocktailError);
  const loading = useSelector(state => state.cocktails.createCocktailLoading);
  const categories = useSelector(state => state.cocktails.categories);


  const onCocktailFormSubmit = data => {
    dispatch(createCocktailRequest(data));
  };
  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h4'>
          New Cocktail
        </Typography>
      </Grid>
      <Grid item xs>
        <CocktailForm
          onSubmit={onCocktailFormSubmit}
          loading={loading}
          error={error}
          categories={categories}
        />
      </Grid>
    </Grid>
  );
};

export default NewCocktail;