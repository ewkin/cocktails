import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Home from "./containers/Home/Home";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import {useSelector} from "react-redux";
import {Helmet} from "react-helmet";
import NewCocktail from "./containers/NewCocktail/NewCocktail";
import Review from "./containers/Review/Review";
import MyCocktails from "./containers/MyCocktails/MyCocktails";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props}/> : <Redirect to={redirectTo}/>;
};

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Layout>
      <Helmet titleTemplate="%s - App"
              defaultTitle="App"/>
      <Switch>
        <ProtectedRoute
          isAllowed={user ? user.role === 'admin' : false}
          redirectTo='/login'
          exact
          path="/cocktail/review"
          component={Review}
        />
        <ProtectedRoute
          isAllowed={user}
          redirectTo='/login'
          path="/cocktail/my" exact component={MyCocktails}
        />
        <Route path="/" exact component={Home}/>
        <Route path="/category/:title" component={Home}/>
        <Route path="/cocktail/add" component={NewCocktail}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
        <Route render={() => <h1>Not found</h1>}/>
      </Switch>
    </Layout>
  );
};

export default App;