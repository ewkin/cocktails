import {put, takeEvery} from "redux-saga/effects";
import {NotificationManager} from "react-notifications";
import {
  createCocktailFailure,
  createCocktailRequest,
  createCocktailSuccess, fetchCocktailApprove, fetchCocktailDelete,
  fetchCocktailsFailure,
  fetchCocktailsRequest, fetchCocktailsReviewRequest,
  fetchCocktailsSuccess, fetchMyCocktails
} from "../actions/cocktailsActions";
import {historyPush} from "../actions/historyActions";
import axiosApi from "../../axiosApi";
import {objectToFormData} from "../../utils";

export function* fetchCocktails({payload: categoryId}) {
  try {
    let url = '/cocktails';
    if (categoryId) {
      url += '?category=' + categoryId;
    }
    const response = yield axiosApi.get(url);
    yield put(fetchCocktailsSuccess(response.data));
  } catch (e) {
    yield put(fetchCocktailsFailure());
    NotificationManager.error('Could not fetch Cocktails')
  }
}

export function* fetchCocktailsReview({payload: categoryId}) {
  try {
    let url = '/cocktails/review/';
    if (categoryId) {
      url += '?category=' + categoryId;
    }
    const response = yield axiosApi.get(url);
    yield put(fetchCocktailsSuccess(response.data));
  } catch (e) {
    yield put(fetchCocktailsFailure());
    NotificationManager.error('Could not fetch Cocktails')
  }
}

export function* fetchMyCocktailsList({payload: categoryId}) {
  try {
    let url = '/cocktails/my/';
    if (categoryId) {
      url += '?category=' + categoryId;
    }
    const response = yield axiosApi.get(url);
    yield put(fetchCocktailsSuccess(response.data));
  } catch (e) {
    yield put(fetchCocktailsFailure());
    NotificationManager.error('Could not fetch Cocktails')
  }
}

export function* approveCocktail({payload: cocktailId}) {
  try {
    let url = '/cocktails/review/';
    yield axiosApi.post(url, cocktailId);
    const response = yield axiosApi.get(url);
    yield put(fetchCocktailsSuccess(response.data));
    NotificationManager.success('Cocktail was Approved')
  } catch (e) {
    NotificationManager.error('Could not approve Cocktails')
    yield put(fetchCocktailsFailure());
  }
}

export function* deleteCocktail({payload: cocktailId}) {
  try {
    let url = '/cocktails/review/';
    yield axiosApi.delete(url+cocktailId);
    const response = yield axiosApi.get(url);
    yield put(fetchCocktailsSuccess(response.data));
    NotificationManager.success('Cocktai was Deleted')
  } catch (e) {
    NotificationManager.error('Could not delete Cocktails')
    yield put(fetchCocktailsFailure());
  }
}


export function* createCocktail({payload: cocktailData}) {
  try {
    const data = objectToFormData(cocktailData);
    yield axiosApi.post('/cocktails', data);
    yield put(createCocktailSuccess());
    yield put(historyPush('/'));
    NotificationManager.success('CocktailView was posted')
  } catch (e) {
    NotificationManager.error('Could not post Cocktails')
    yield put(createCocktailFailure(e.response.data));
  }
}

const cocktailsSagas = [
  takeEvery(fetchCocktailsRequest, fetchCocktails),
  takeEvery(createCocktailRequest, createCocktail),
  takeEvery(fetchCocktailsReviewRequest, fetchCocktailsReview),
  takeEvery(fetchMyCocktails, fetchMyCocktailsList),
  takeEvery(fetchCocktailApprove, approveCocktail),
  takeEvery(fetchCocktailDelete, deleteCocktail)
];

export default cocktailsSagas;

