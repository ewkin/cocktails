import cocktailsSlice from "../slices/cocktailsSlice";


export const {
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchCocktailsFailure,
  createCocktailSuccess,
  createCocktailRequest,
  createCocktailFailure,
  fetchCocktailsReviewRequest,
  fetchMyCocktails,
  fetchCocktailApprove,
  fetchCocktailDelete
} = cocktailsSlice.actions;