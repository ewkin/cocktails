import {createSlice} from "@reduxjs/toolkit";

const name = 'cocktails';
const cocktailsSlice = createSlice({
  name,
  initialState: {
    cocktails: [],
    categories: [{title: 'Alcoholic'}, {title: 'Non-Alcoholic'}],
    cocktailsLoading: false,
    createCocktailLoading: false,
    createCocktailError: null
  },
  reducers: {
    fetchCocktailsRequest: state => {
      state.cocktailsLoading = true;
    },
    fetchCocktailsReviewRequest: state => {
      state.cocktailsLoading = true;
    },
    fetchCocktailApprove: state => {
      state.cocktailsLoading = true;
    },
    fetchCocktailDelete: state => {
      state.cocktailsLoading = true;
    },
    fetchMyCocktails: state => {
      state.cocktailsLoading = true;
    },
    fetchCocktailsSuccess: (state, {payload: cocktails}) => {
      state.cocktailsLoading = false;
      state.cocktails = cocktails;
    },
    fetchCocktailsFailure: state => {
      state.cocktailsLoading = false;
    },
    createCocktailRequest: state => {
      state.createCocktailLoading = true;
    },
    createCocktailSuccess: state => {
      state.createCocktailLoading = false;
    },
    createCocktailFailure: (state, {payload: error}) => {
      state.createCocktailLoading = false;
      state.createCocktailError = error;
    }
  }
});

export default cocktailsSlice;