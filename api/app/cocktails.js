const express = require('express');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Cocktail = require("../models/Cocktail");
const config = require('../config');
const upload = require('../multer').cocktails;


const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const criteria = {published: true};
    if (req.query.category) {
      criteria.category = req.query.category;
    }
    const cocktails = await Cocktail.find(criteria);
    res.send(cocktails);
  } catch (e) {
    res.sendStatus(500);
  }

});


router.get('/review/', auth, permit('admin'), async (req, res) => {
  try {
    const criteria = {};
    if (req.query.category) {
      criteria.category = req.query.category;
    }
    const cocktails = await Cocktail.find(criteria);
    res.send(cocktails);

  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/review/', auth, permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findOne({_id: req.body.id});
    cocktail.published = true;
    cocktail.save();
    res.send(cocktail);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/review/:id', auth, permit('admin'), async (req, res) => {
  try {
     await Cocktail.deleteOne({_id: req.params.id});

    res.send({message: "Deleted"});
  } catch (e) {
    res.sendStatus(500);
  }
});
router.get('/my/', auth, async (req, res) => {
  try {
    const criteria = {user: req.user._id};
    if (req.query.category) {
      criteria.category = req.query.category;
    }
    const cocktails = await Cocktail.find(criteria);
    res.send(cocktails);

  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const cocktailData = {
      category: req.body.category || null,
      name: req.body.name,
      recipe: req.body.recipe,
      ingredients: req.body.ingredients,
      user: req.user._id,
      image: req.file ? config.url + req.file.filename : null
    };


    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();

    res.send({cocktail});
  } catch (e) {
    res.status(400).send(e);
  }

});

module.exports = router;