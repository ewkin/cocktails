const mongoose = require('mongoose');

const IngredientSchema = new mongoose.Schema({
  title: String,
  amount: String
});

const RatingSchema = new mongoose.Schema({
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  rating: {type: Number, min: 1, max: 5},
});


const CocktailSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  recipe: String,
  published: {
    type: Boolean,
    required: true,
    default: false,
  },
  category: {
    type: String,
    required: true,
    enum: ['Alcoholic', 'Non-Alcoholic']
  },
  image: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  ingredients: [IngredientSchema],
  rating: [RatingSchema]
});


const Cocktail = mongoose.model('Cocktail', CocktailSchema);
module.exports = Cocktail;